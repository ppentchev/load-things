# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

[build-system]
requires = ["setuptools >= 61", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "load-things"
dynamic = ["dependencies", "version"]
description = "Parse a Nox environments file and output the session attributes."
readme = "README.md"
license = {text = "BSD-2-Clause"}
requires-python = ">= 3.8"
classifiers = [
  "Development Status :: 5 - Production/Stable",
  "Environment :: Console",
  "Intended Audience :: Developers",
  "License :: DFSG approved",
  "License :: Freely Distributable",
  "License :: OSI Approved :: BSD License",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3 :: Only",
  "Programming Language :: Python :: 3.8",
  "Programming Language :: Python :: 3.9",
  "Programming Language :: Python :: 3.10",
  "Programming Language :: Python :: 3.11",
  "Topic :: Software Development :: Build Tools",
  "Topic :: Software Development :: Libraries",
  "Topic :: Software Development :: Libraries :: Python Modules",
  "Topic :: Utilities",
  "Typing :: Typed",
]

[[project.authors]]
name = "Peter Pentchev"
email = "roam@ringlet.net"

[project.scripts]
load-things = "load_things.__main__:main"

[project.urls]
Source = "https://gitlab.com/ppentchev/load-things"

[tool.setuptools]
zip-safe = true
package-dir = {"" = "src"}
packages = ["load_things"]

[tool.setuptools.dynamic]
dependencies = {file = "requirements/install.txt"}
version = {attr = "load_things.VERSION"}

[tool.setuptools.package-data]
load_things = ["py.typed"]

[tool.black]
target-version = ["py38", "py39", "py310", "py311"]
line-length = 100

[tool.mypy]
strict = true
python_version = "3.8"

[tool.ruff]
line-length = 100
select = ["ALL"]
ignore = [
  "ANN101",
  "COM812",
  "D203",
  "D213",
]

[tool.ruff.isort]
force-single-line = true
known-first-party = ["load_things"]
lines-after-imports = 2
single-line-exclusions = ["typing"]

[tool.ruff.per-file-ignores]
# This is a console test tool, output is what it does
"loaded_*dataclass.py" = ["T201"]

[tool.test-stages]
stages = ["ruff and not @manual", "@check and not @manual", "@tests and not @manual"]
