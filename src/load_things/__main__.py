# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Actually try to load the thing."""

from __future__ import annotations

import importlib.machinery as imp_mach
import importlib.util as imp_util
import pathlib
import typing

import click


if typing.TYPE_CHECKING:
    import types
    from typing import Final


def load_the_thing(path: pathlib.Path, *, from_source: bool) -> types.ModuleType:
    """Load the module."""
    loader: Final = imp_mach.SourceFileLoader("loaded", str(path))

    spec: Final = (
        imp_util.spec_from_loader(loader.name, loader, origin=str(path), is_package=False)
        if from_source
        else imp_util.spec_from_file_location(loader.name, path, loader=loader)
    )
    assert spec is not None  # noqa: S101  # mypy needs this

    mod: Final = imp_util.module_from_spec(spec)
    loader.exec_module(mod)
    return mod


@click.command(name="load-things", help="Load a thing or two")
@click.option(
    "--from-source", is_flag=True, help="Use a SourceFileLoader instead of spec_from_location()"
)
@click.argument(
    "path",
    type=click.Path(exists=True, dir_okay=False, path_type=pathlib.Path, resolve_path=True),
)
def main(*, from_source: bool, path: pathlib.Path) -> None:
    """Load the thing."""
    mod: Final = load_the_thing(path, from_source=from_source)
    if hasattr(mod, "say_something"):
        mod.say_something("hello there")
    else:
        obj: Final = mod.Something("hello there")
        obj.say_something()
