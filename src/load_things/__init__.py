# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Try loading another module."""

VERSION = "0.1.0"
"""The version of the whole test setup."""
