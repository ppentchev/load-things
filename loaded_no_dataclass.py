# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A file that should be loaded by another file."""

from __future__ import annotations


def say_something(something: str) -> None:
    """Say something."""
    print(f"We were told to say {something}")
