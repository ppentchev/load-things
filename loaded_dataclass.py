# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A file that should be loaded by another file."""

from __future__ import annotations

import dataclasses


@dataclasses.dataclass
class Something:
    """A dataclass that says something."""

    something: str
    """The thing to say."""

    def say_something(self) -> None:
        """Say something."""
        print(f"We were told to say {self.something}")
